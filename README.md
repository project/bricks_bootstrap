# Bricks <sub><sup>for</sup></sub> Bootstrap

Set of pre-defined [bricks](https://www.drupal.org/project/bricks) and templates for Drupal 8 and Bootstrap 4.


## Live demo

1. Open [pre-configured sandbox](https://simplytest.me/project/bricks_bootstrap).
2. Click **Launch sandbox** and wait.
3. Follow the installation (all settings should be pre-filled, don't change them).
4. Go to **Extend** and install **Bricks Bootstrap**.
5. Go to **Appearance**, install **Tweme** and set as default theme.
6. Find a sample node and go to **Edit** mode to check magic out!
7. Finally go to **Content** and create your own first **Bricky** page!
